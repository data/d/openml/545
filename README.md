# OpenML dataset: lmpavw

https://www.openml.org/d/545

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

-------------------------------------------------------------------------------

TIME SERIES USED IN
LONG-MEMORY PROCESSES, THE ALLAN VARIANCE AND WAVELETS
BY D. B. PERCIVAL AND P. GUTTORP, A CHAPTER IN
WAVELETS IN GEOPHYSICS
EDITED BY E. FOUFOULA-GEORGIOU AND P. KUMAR, ACADEMIC PRESS, 1994.
-------------------------------------------------------------------------------

VERTICAL OCEAN SHEAR "TIME" SERIES (SHOWN IN FIGURE 1 OF CHAPTER)
SOURCE: APPLIED PHYSICS LABORATORY, UNIVERSITY OF WASHINGTON (MIKE GREGG)
SAMPLING INTERVAL (DELTA T):           0.1 METERS
SAMPLE SIZE: 6875
DEPTH ("TIME") OF FIRST DATA POINT:  350.0 METERS
DEPTH          OF  LAST DATA POINT: 1037.4 METERS


Information about the dataset
CLASSTYPE: numeric
CLASSINDEX: 1

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/545) of an [OpenML dataset](https://www.openml.org/d/545). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/545/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/545/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/545/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

